/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class HouseMouse extends Animal{

    public HouseMouse() {
        this.species = "Mouse";
        this.max_speed = 21;
        this.max_age = 13;
        this.moving_type = "scurry";
    }
}
