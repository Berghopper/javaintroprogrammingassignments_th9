/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {
    // first make all constants that all classes will have.
    public String species = "";
    public double max_speed = 0;
    // define max age as 1 to bypass division by zero.
    public int max_age = 69;
    public int age = 0;
    public String moving_type = "";


    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        return species;
    }
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return moving_type;
    }
    
    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed() {
        return max_speed * (0.5 + (0.5 * ((double)(max_age - age) / max_age)));
    }
    
}
