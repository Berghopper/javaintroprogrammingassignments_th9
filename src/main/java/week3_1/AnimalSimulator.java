/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

import java.util.*;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class AnimalSimulator {
    private static final Map<String, Animal> supported_animals;
    static
    {
        supported_animals = new HashMap<String, Animal>();
        supported_animals.put("Elephant", new Elephant());
        supported_animals.put("Horse", new Horse());
        supported_animals.put("Mouse", new HouseMouse());
        supported_animals.put("Tortoise", new Tortoise());
    }

    public static void main(String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
        anSim.start(args);
    }

    
    private void start(String[] args) {
        //start processing command line arguments
        //first check for help or see if the input is empty
        boolean goodargs = true;
        if (args.length < 1){
            print_help();
            goodargs = false;
//            System.exit(0);
        }
        else if (args[0].equals("help")){
            print_help();
            goodargs = false;
//            System.exit(0);
        }
        //then check if the amount of arguments is even, else arguments can't be unpacked
        else if ((args.length % 2) != 0) {
            System.out.println("Incorrect amount of arguments given!, please run with single parameter \"help\" to " +
                     "see what options are available and how to run this program.");
            goodargs = false;
//            System.exit(1);
        }
        //Iterate through the arguments if args are doin ok.
        if (goodargs) {
            for (int i = 0; i < (args.length - 1); i += 2) {
                boolean correct_animal = true;
                String animal = args[i];
                int animal_age = Integer.parseInt(args[i + 1]);
                //Try to grab the correct object from the Hashmap, if not available, print an error and exit.
                Animal animal_object = supported_animals.get(animal);
                if (animal_object == null) {
                    // if animal isn't found, make user refer to the help page.
                    System.out.println("Error: animal species " + animal + " is not known. run with single parameter" +
                            " \"help\" to get a listing of available species. Please give new values");
                    correct_animal = false;
                }
                // if the animal_object does contain stuff, continue
                //before inputting the given age, check if it is equal or lower than the max age
                if (correct_animal) {
                    if (animal_age > animal_object.max_age) {
                        System.out.println("Error: maximum age of " + animal + " is " +
                                Integer.toString(animal_object.max_age) + " years. Please give new values");
                        correct_animal = false;
                    }
                }
                // if nothing is fishy, just input the correct age, and make a nice string displaying info about the
                // animal
                if (correct_animal) {
                    animal_object.age = animal_age;
                    // define the correct article depending if the animal has a consonant or not.
                    String article = "A";
                    if (isVowel((Character.toString(animal.charAt(0))))) {
                        article = "An";
                    }

                    System.out.println(String.format("%s %s of age %d moving in %s at %.1f km/h", article,
                            animal_object.getName(), animal_object.age, animal_object.getMovementType(),
                            animal_object.getSpeed()));
                }
            }
        }
    }
    
    /**
     * returns all supported animals as List, alhabetically ordered
     * @return supportedAnimals the supported animals
     */
    public List<String> getSupportedAnimals() {
        List<String> thelist = new ArrayList<>(supported_animals.keySet());
        Collections.sort(thelist);
        return thelist;
    }

    public static boolean isVowel(String c){
        String vowels = "aeiouAEIOU";
        return vowels.contains(c);
    }

    private void print_help(){
        System.out.println("Usage: java AnimalSimulator <Species age Species age ...>");
        System.out.println("Supported species (in alphabetical order):");
        List supported_animals_list = getSupportedAnimals();
        for (int i = 0; i < supported_animals_list.size(); i++) {
            System.out.println(Integer.toString(i+1)+": "+supported_animals_list.get(i));
        }
    }
}
