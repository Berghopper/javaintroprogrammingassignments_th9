/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Horse extends Animal {

    public Horse() {
        this.species = "Horse";
        this.max_speed = 88;
        this.max_age = 62;
        this.moving_type = "gallop";
    }

}

