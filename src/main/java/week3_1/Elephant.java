/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Elephant extends Animal {

    public Elephant() {
        this.species = "Elephant";
        this.max_speed = 40;
        this.max_age = 86;
        this.moving_type = "thunder";
    }

}
