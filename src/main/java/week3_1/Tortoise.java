/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Tortoise extends Animal {

    public Tortoise() {
        this.species = "Tortoise";
        this.max_speed = 0.3;
        this.max_age = 190;
        this.moving_type = "crawl";
    }
}
