/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week2_1;

import java.util.*;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {

    /**
     * testing main.
     * @param args 
     */
    public static void main(String[] args) {
        String[] sequences = new String[4];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GAAC";

        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus = csc.createConsensus(sequences, true);
        System.out.println(consensus);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        System.out.println(consensus);
        //consensus should equal "GA[A/T][A/T/C]"
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     * @param sequences the sequences to scan for consensus
     * @param iupac flag to indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public String createConsensus(String[] sequences, boolean iupac) {
        Map<Set, String> iupac_map = new HashMap<>();
        iupac_map.put(new HashSet<String>(Arrays.asList("A", "C", "G", "T")), "N");
        iupac_map.put(new HashSet<String>(Arrays.asList("A")), "A");
        iupac_map.put(new HashSet<String>(Arrays.asList("G")), "G");
        iupac_map.put(new HashSet<String>(Arrays.asList("C")), "C");
        iupac_map.put(new HashSet<String>(Arrays.asList("T")), "T");
        iupac_map.put(new HashSet<String>(Arrays.asList("A", "T")), "W");
        iupac_map.put(new HashSet<String>(Arrays.asList("C", "G")), "S");
        iupac_map.put(new HashSet<String>(Arrays.asList("A", "C")), "M");
        iupac_map.put(new HashSet<String>(Arrays.asList("G", "T")), "K");
        iupac_map.put(new HashSet<String>(Arrays.asList("A", "G")), "R");
        iupac_map.put(new HashSet<String>(Arrays.asList("C", "T")), "Y");
        iupac_map.put(new HashSet<String>(Arrays.asList("C", "G", "T")), "B");
        iupac_map.put(new HashSet<String>(Arrays.asList("A", "G", "T")), "D");
        iupac_map.put(new HashSet<String>(Arrays.asList("A", "C", "T")), "H");
        iupac_map.put(new HashSet<String>(Arrays.asList("A", "C", "G")), "V");


        int no_sequences = sequences.length;
        int individual_length = sequences[0].length();
        Set[] char_container = new Set[individual_length];

        // first loop through character 1 through n
        for (int i = 0; i < individual_length; i++) {
            // first define indiv_chars as an empty string
            Set indiv_chars = new TreeSet();
            // then loop through all the actual sequences and just add them.
            for (int j = 0; j < no_sequences; j++) {
                indiv_chars.add(sequences[j].substring(i,i+1));
            // after the individual chars for the current position have been added, make the string unique and add
            // to the global char_container
            char_container[i] = (indiv_chars);
            }
        }
        // after the chars of each index have been figured out, see how it needs to be formatted.
        String final_formatted_string = "";
        if (iupac){
            // for each char, add the iupac code
            for (int i = 0; i < individual_length; i++) {
                final_formatted_string += iupac_map.get(char_container[i]);
            }
        }
        else {
            // for each char, check if there are multiple, if so, add a formatted substring to the final one.
            for (int i = 0; i < individual_length; i++) {
                if (char_container[i].size() == 1){
                    Iterator<String> nuc_char = char_container[i].iterator();
                    while(nuc_char.hasNext()){
                        final_formatted_string += nuc_char.next();
                    }
                }

                else{
                    String char_combination = "[";
                    Boolean first_char_placed = false;
                    Iterator<String> nuc_char = char_container[i].iterator();
                    while(nuc_char.hasNext()){
                        if (!first_char_placed){
                            first_char_placed = true;
                            char_combination += nuc_char.next();
                        }
                        else {
                            char_combination += "/"+nuc_char.next();
                        }
                    }
                    char_combination += "]";
                    final_formatted_string += char_combination;
                }

            }
        }

        return final_formatted_string;
    }
}
