package week1_0;

/**
 * Creation date: Jun 26, 2017
 *
 * @version 0.01
 * @author Michiel Noback (&copy; 2017)
 * @author Casper Peters (some edits, and answers)
 */

public class StartingJava {
    public static void main(String[] args) {
        StartingJava sj = new StartingJava();
        sj.printHelloWorld();
        System.out.println(" ");
        System.out.println(sj.addInts(2, 3));
        System.out.println(sj.divideAndRound(12.3, 1.3));
        System.out.println(sj.getGreeting(2));
        System.out.println(sj.createDuck(69, "420blazit"));
    }
    public static final String[] GREETINGS = new String[]{
            "Hallo",
            "Moi",
            "Wazzup",
            "Yo!",
            "Hey"
    };

    /**
     * Method simply prints "Hello, World" to the console.
     *
     */
    public void printHelloWorld() {
        System.out.print("Hello, World");
    }

    /**
     * returns the sum of x and y
     * @param x
     * @param y
     * @return theSum
     */
    public int addInts(int x, int y) {
        return x + y;
    }

    /**
     * divides x by y and returns the rounded value (rounded to nearest integer)
     * @param x
     * @param y
     * @return dividedAndRounded
     */
    public long divideAndRound(double x, double y) {
        //Math may be a nice class here!
        return Math.round(x/y);
    }

    /**
     * returns the string found at the corresponding position of GREETINGS
     * @param index of the greeting
     * @return greeting
     */
    public String getGreeting(int index) {
        return GREETINGS[index];
    }


    /**
     * Returns a Duck object with the given.
     * The Duck class is already present in this same package.
     * @param swimSpeed
     * @param nameOfDuck
     * @return duck
     */
    public Duck createDuck(int swimSpeed, String nameOfDuck) {
        Duck ducky = new Duck();
        ducky.name = nameOfDuck;
        ducky.swimSpeed = swimSpeed;
        return ducky;
    }

}
