/*
 * Copyright (c) 2014 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package week1_2;

/**
 *
 * @author michiel
 */
public class AllSubstringsPrinter {
    /**
     * main method serves development purposes only.
     * @param args the args
     */
    public static void main(final String[] args) {
        AllSubstringsPrinter asp = new AllSubstringsPrinter();
        asp.printAllSubstrings("GATCG", true, true); //should print left truncated, left aligned
    }

    /**
     * will print all possible substrings according to arguments.
     * @param stringToSubstring the string to substring
     * @param leftTruncated flag to indicate whether the substrings should be truncated from the left (or the right)
     * @param leftAligned flag to indicate whether the substrings should be printed left-aligned (or right-aligned)
     */
    public void printAllSubstrings(
            final String stringToSubstring,
            final boolean leftTruncated,
            final boolean leftAligned) {
        //your code
        for(int i=stringToSubstring.length(); i>0; i--){
            //define for the case of righttruncation
            int endindex = i; // default i, is endindex so it cuts off from the right.
            int startindex = 0;
            int nochar = i; // number of chars in substring
            //change if lefttruncated is true.
            if(leftTruncated){
                endindex = stringToSubstring.length(); //with lefttruncation, conserve end
                startindex = stringToSubstring.length()-i; //but make the startindex increment
            }
            String non_aligned = stringToSubstring.substring(startindex, endindex);
            //if leftAligned is true there's no need to do anything because that's the default.
            //however, if it is false we need to add spaces. we know the amount of spaces needed by
            //subtracting the no. chars from the total original length.
            String finished_substring = non_aligned;
            if(!leftAligned){
                int spacesneeded = stringToSubstring.length() - nochar;
                //format a string to fit all the spaces
                String spaces = new String();
                //only add spaces when spacesneeded > 0
                if(spacesneeded > 0){
                    spaces = String.format("%"+spacesneeded+"s", "");
                }
                //finally, redefine the finished substring.
                finished_substring = spaces + non_aligned;
            }
            //finally print the finished string
            System.out.println(finished_substring);

        }
    }
}
