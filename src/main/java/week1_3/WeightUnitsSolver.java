/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week1_3;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class WeightUnitsSolver {
    /**
     * main to be used for testing purposes
     * @param args 
     */
    public static void main(String[] args) {
        WeightUnitsSolver wus = new WeightUnitsSolver();
        System.out.println(wus.convertFromGrams(1000));
    }
    
    /**
     * will return the number of Pounds, Ounces and Grams represented by this quantity of grams, 
     * encapsulated in a BritishWeightUnits object.
     * @param grams
     * @return a BritishWeightUnits instance
     * @throws IllegalArgumentException when the Grams quantity is 
     */
    public BritishWeightUnits convertFromGrams(int grams) {
        //check for positive amount of grams
        if (grams <= 0) {
            throw new IllegalArgumentException("Error: The given amount of grams is 0 or below " +
                    "(should at least be higher than 0). Given: grams="
                    + grams);
        }
        //check amount of pounds
        int pounds = Math.round(grams/454);
        //reduce leftover grams with modulo
        grams = Math.round(grams%454);
        //check amount of ounces
        int ounces = Math.round(grams/28);
        //reduce leftover grams with modulo
        grams = Math.round(grams%28);
        return new BritishWeightUnits(pounds, ounces, grams);
    }
}
